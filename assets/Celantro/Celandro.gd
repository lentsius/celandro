extends KinematicBody

#STATS
var lives : int = 3
var times_jumped : int = 0
var shoes_collected : int = 0
var objects_dodged : int = 0

var invincible : = false

var speed : float = 0
var max_speed : int = 1
var jump : = false
var jump_force = 40

var gravity : float = -9.8 / 6

var colliding : = false

var velocity : Vector3
var floored : = false

var smooth_rot : Vector2
var target_rot : Vector2

var target_x : float = -54.81
var speed_z : float
var target_z : float = 0.0
var smooth_y : float = -180.0
var smooth_x : float = -54.81
signal info(pos, rot)

onready var normal_face = preload('res://assets/Celantro/res/EyeTextures/EyeNormal.png')
onready var jump_face = preload('res://assets/Celantro/res/EyeTextures/EyeJump.png')
onready var fall_face = preload('res://assets/Celantro/res/EyeTextures/EyeFall.png')
onready var run_face = preload('res://assets/Celantro/res/EyeTextures/EyeRun.png')
onready var face01 = preload('res://assets/Celantro/res/EyeTextures/EyeInbetween01.png')
onready var face02 = preload('res://assets/Celantro/res/EyeTextures/EyeInbetween02.png')
onready var face03 = preload('res://assets/Celantro/res/EyeTextures/EyeInbetween03.png')
onready var face04 = preload('res://assets/Celantro/res/EyeTextures/EyeInbetween04.png')

var between_faces : Array
var current_face

var lanes : Array = [0,1,2]
var current_lane : = 1

var disabled_casts : Array = []

var cols : Array

signal changed_lane(new_lane_number)
signal jumped
signal place_footstep(location, y)
signal speed(amount)
signal collided
signal announce(message, col)
signal stats(lives, times_jumped, shoes_collected, objects_dodged)

func _ready():
	between_faces.append(face01)
	between_faces.append(face02)
	between_faces.append(face03)
	between_faces.append(face04)
	move_lock_x = true
#	start_timer(1)

func _process(delta):
	if Input.is_action_just_pressed("Left"):
		change_lane(-1)
		change_face(normal_face)	
			
	elif Input.is_action_just_pressed("Right"):
		change_lane(1, 1)
		change_face(normal_face)
		
	
	if $Body/FloorCast.is_colliding():
		floored = true
		
		#switch faces should they not be what they should be
		if current_face != run_face and speed > 0.5:
			change_face(run_face)
			# ADD AWESOME PARTICLES HERE
			
		elif current_face != normal_face and speed < 0.5:
			change_face(normal_face)
			
	else:
		floored = false
	
	if $Body/RayCast.is_colliding():
		emit_signal("collided")
		colliding = true
		$Body/RayCast.get_collider().leave()
		lose_life()
		#Destroy the walls!!! if you want...
		
#		print($Body/RayCast.get_collider().name)
#		$Body/RayCast.get_collider().leave()
		
	else:
		colliding = false
	
	speed_z = speed * 20
	emit_signal("speed", speed)
	$Sounds/Wind.volume_db = -61 + (speed * 17)

func _physics_process(delta):
	global_transform.origin.x = 0
	global_transform.origin.z = 0
#	move_lock_z = true
	smooth_rot = smooth_rot.linear_interpolate(target_rot, 1 * delta)
	if Input.is_action_just_pressed("jump") and floored:
		velocity.y = 0
		velocity.y += jump_force
		change_face(jump_face, 0.05)
		emit_signal("jumped")
		times_jumped += 1
		emit_signal("stats", lives, times_jumped, shoes_collected, objects_dodged)

	if !colliding:
	
		if speed < max_speed:
			speed += 0.0003
		
		rotate_y(1 * speed * delta)
		if floored:
			$Body/Feet.rotate_object_local(Vector3(1,0,0), -(10 * (speed / 2) * delta))
			target_rot = Vector2(9,9)
		
		$Body/Feet.rotate_object_local(Vector3(1,0,0), -(smooth_rot.x * speed * delta))
		
	if !floored:
		velocity.y += gravity
		if velocity.y < 0:
			change_face(fall_face)
			
		target_rot = Vector2(0,0)
		
	else:
		control_step()
		
	move_and_slide(velocity)
	emit_signal("info", global_transform.origin, rotation)
	
	# diff = + 1 or -1 depending on whether we're moving up or down a lane
	# side is whether we're going left or right - 0 = left ad 1 = right
	# yes I'm left handed
func change_lane(diff : int, side : int = 0) -> void:
	if current_lane + diff < 0 or current_lane + diff > 2:
		return
	current_lane += diff
	match current_lane:
		0:
			target_x = -51.81
		1:
			target_x = -54.81
		2:
			target_x = -57.81
			
	match side:
		0:
			target_z = 10 + speed_z
		1:
			target_z = -10 + speed_z
			
	update_collision()
			
	var new_transl = $Body.translation
	new_transl.x = target_x
	
	var starting_rot = $Body.rotation_degrees
	starting_rot.z = target_z
	var fin_rot = $Body.rotation_degrees
	fin_rot.z = speed_z
	
	$Tween.interpolate_property($Body, "translation", $Body.translation, new_transl, 0.75, Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
#	$Tween.interpolate_property($CollisionShape, "translation", $Body.translation, new_transl, 0.75, Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
	$Tween.interpolate_property($Body, "rotation_degrees", fin_rot,starting_rot, 0.25, Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
	$Tween.interpolate_property($Body, "rotation_degrees", starting_rot, fin_rot, 1.5, Tween.TRANS_ELASTIC, Tween.EASE_OUT, 0.25)
	$Tween.start()
	
	emit_signal('changed_lane', current_lane)
	
func update_collision() -> void:
	cols.clear()
	cols.append($col0)
	cols.append($col1)
	cols.append($col2)
	for i in cols:
		i.disabled = true
		i.translation.y = 50
	
	cols[current_lane].disabled = false
	cols[current_lane].translation.y = 0
	
func control_step() -> void:
	for i in $Body/Feet/Raycasts.get_children():
		if i.is_colliding() && i is RayCast:
			i.enabled = false
			$Body/Footstep.pitch_scale = rand_range(0.5, 1.5)
			$Body/Footstep.play() 
			emit_signal("place_footstep", i.get_collision_point(), rotation_degrees.y)
			disabled_casts.append(i)
			
	
func change_face(new_face, speed : float = 0.1) -> void:
	var mat : Material = $Body/Eyes/Cylinder010.get_surface_material(0)
	between_faces.shuffle()
	for i in range(0,3):
		mat.set_shader_param('eye_texture', between_faces[i])
		yield(get_tree().create_timer(speed), 'timeout')
	mat.set_shader_param('eye_texture', new_face)
	current_face = new_face

func start_timer(wait_time : float = 0.3) -> void:
	$Step.wait_time = wait_time
	$Step.start()

func _on_Step_timeout() -> void:
	$ActivateCasts.wait_time = 1.1 - (speed * 1.09)
	if disabled_casts.size() > 0:
		disabled_casts[0].enabled = true
		disabled_casts.remove(0)

func _on_PickUps_body_entered(body) -> void:
	if body.is_in_group("shoe"):
		body.collect()
		$Sounds/Coin.play()
		shoes_collected += 1
		emit_signal("stats", lives, times_jumped, shoes_collected, objects_dodged)
	else:
		lose_life()
		
func lose_life() -> void:
	if invincible == true:
		return
	invincible = true
	$Invincible.start()
	if lives > 0:
		speed = 0.2
		lives -= 1
		emit_signal("announce", "OUCH!", Color.darkred)
		emit_signal("stats", lives, times_jumped, shoes_collected, objects_dodged)
	else:
		get_parent().end_game()
		
func dodged_object(no : int = 0) -> void:
	objects_dodged += no
	emit_signal("stats", lives, times_jumped, shoes_collected, objects_dodged)

func _on_Invincible_timeout():
	invincible = false