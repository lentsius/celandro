extends Area



func _on_Area_body_entered(body):
	body.disappear()
	$Pass.pitch_scale = rand_range(0.8,1.2)
	$Pass.play()
	get_parent().dodged_object(1)