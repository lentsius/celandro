extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Announcement.modulate.a = 0
	$Vertical/Ujo/Player.text = globals.player_name

func _on_Celandro_speed(amount) -> void:
	$Vertical/Bg/HBoxContainer/Speed.text = str(amount * 100) + " %"

func _on_CameraJoint_announce(message, col) -> void:
	announce(message, col)
	
func announce(what : String, col : Color) -> void:
	$Announcement.text = what
	$Announcement.modulate = col
	var color_transparent = col
	color_transparent.a = 0
	$Tween.interpolate_property($Announcement, "rect_scale", Vector2(0.01,0.01), Vector2(0.5,0.5), 1,Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property($Announcement, "rect_scale", Vector2(0.5,0.5), Vector2(1.0,1.0), 1,Tween.TRANS_EXPO, Tween.EASE_OUT, 1.5)
	$Tween.interpolate_property($Announcement, "modulate", color_transparent, col, 1,Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property($Announcement, "modulate", col, color_transparent, 1,Tween.TRANS_EXPO, Tween.EASE_OUT, 1.5)
	$Tween.start()

func _on_Celandro_stats(lives, times_jumped, shoes_collected, objects_dodged):
	$Vertical/HBoxContainer2/Collected.text = str(shoes_collected)
	$Vertical/HBoxContainer3/Dodged.text = str(objects_dodged)
	$Vertical/HBoxContainer4/Jumped.text = str(times_jumped)
	
	var unu = $"Vertical/Ujo/1"
	var du = $"Vertical/Ujo/2"
	var tri = $"Vertical/Ujo/3"
	
	match lives:
		3:
			unu.visible = true
			du.visible = true
			tri.visible = true
		2:
			unu.visible = true
			du.visible = true
			tri.visible = false
		1:
			unu.visible = true
			du.visible = false
			tri.visible = false
		0:
			unu.visible = false
			du.visible = false
			tri.visible = false