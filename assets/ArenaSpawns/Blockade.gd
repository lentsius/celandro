extends StaticBody

func initiate() -> void:
	$Tween.interpolate_property($Mesh, "scale", Vector3(0.1,0.1,0.1), Vector3(1,1,1), 2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start()

func shrink() -> void:
	$Mesh.scale = Vector3(0.1,0.1,0.1)

func disappear() -> void:
	leave()
	
func leave() -> void:
	$CollisionShape.disabled = true
	$Tween.interpolate_property($Mesh, "scale", Vector3(1,1,1), Vector3(0.1,0.1,0.1), 2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property($Mesh, "translation", $Mesh.translation, Vector3($Mesh.translation.x, $Mesh.translation.y + 30 ,$Mesh.translation.z), 2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start()
	yield($Tween, "tween_completed")
	queue_free()