tool
extends ColorRect

var original_size : Vector2
var original_font_loc : Vector2
export(float, 0, 500) var growth_amount : float = 20

export(String) var text : String = "PLAY" setget set_text
export var horizontal : bool = false
export(Color, RGBA) var hover_color = Color.coral
export(Color, RGBA) var active_color = Color.dimgray
export(Color, RGBA) var base_color = Color.gray
export var animated_text : bool = false
export(float, 0, 50) var anim_text_offset : float = 0

signal button_pressed

func _ready():
#	$Label.rect_position.y += (rect_size.y / 2)
	original_font_loc = $Label.rect_position
	color = base_color
#	$Label.text = text
	original_size = rect_size
	rect_pivot_offset = rect_size / 2

func set_text(new):
	$Label.text = new

func _on_BaseButton_mouse_entered():
	var bigger : Vector2
	if !horizontal:
		bigger = Vector2(original_size.x + growth_amount, original_size.y + growth_amount)
	else:
		bigger = Vector2(original_size.x + growth_amount, original_size.y)
		
	$Tween.interpolate_property(self, "modulate", Color(1,1,1,0.7), Color(1,1,1,1), 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property(self, "color", base_color, hover_color, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property(self, "rect_size", original_size, bigger, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)	
	
	if animated_text:
		$Tween.interpolate_property($Label, "rect_position", original_font_loc, Vector2(original_font_loc.x + anim_text_offset, original_font_loc.y), 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	
	$Tween.start()


func _on_BaseButton_mouse_exited():
	$Tween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0.7), 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property(self, "color", hover_color, base_color, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)	
	$Tween.interpolate_property(self, "rect_size", rect_size, original_size, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	
	if animated_text:
		$Tween.interpolate_property($Label, "rect_position", $Label.rect_position, original_font_loc, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)	
	
	$Tween.start()


# mouse click
func _on_BaseButton_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == 1 and event.pressed:
			$Tween.stop(self, "color")
			color = active_color
		elif event.button_index == 1 and !event.pressed:
			color = hover_color
			emit_signal("button_pressed")
