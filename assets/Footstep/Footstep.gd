extends Sprite3D
	
var wait_time : float = 2.0
	
	
func initiate() -> void:
	$Tween.interpolate_property(self, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start()
	
	
func into_the_pool() -> void:
	get_parent().pool.append(self)
	self.translation.z = - 1000
	
	
func _on_Tween_tween_completed(object, key):
	into_the_pool()