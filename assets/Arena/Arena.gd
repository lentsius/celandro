extends Spatial

var diameter : float = 5.47
var orig_modulate : Color
var new_modulate : Color = Color(1,1,1,0.5)

func _ready():
	orig_modulate = $Lane.modulate
	_on_Celandro_changed_lane(1)

func _on_Celandro_changed_lane(new_lane_number):
	match new_lane_number:
		0:
			diameter = 5.18
		1:
			diameter = 5.47
		2:
			diameter = 5.77
			
	var new_scale = Vector3(diameter, diameter, 1.0)
	$Tween.interpolate_property($Lane, 'scale', $Lane.scale, new_scale, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property($Lane, 'modulate', new_modulate, orig_modulate, 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()
	
	$Sounds/ChangeLane.play()