shader_type canvas_item;

uniform float distort_factor;

void fragment(){
	vec2 uv = SCREEN_UV;
	uv.y += distort_factor * (-2.0 * uv.y + 1.0) * uv.x * (uv.x - 1.0);
	vec4 backdrop = texture(SCREEN_TEXTURE, uv);
	COLOR = backdrop;
}