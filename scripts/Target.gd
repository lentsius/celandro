extends Spatial

var speed : float = 0
var max_speed : int = 2

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if speed < max_speed:
		speed += 0.001
		
	rotate_y(1 * speed * delta)
