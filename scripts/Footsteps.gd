extends Spatial

var pool : Array

func _ready():
	create_footstep(100)

func create_footstep(how_many) -> void:
	var step_base = preload('res://assets/Footstep/Footstep.tscn')
	for i in range(0,how_many):
		var step = step_base.instance()
		step.translation = Vector3(0,0,-1000)
		pool.append(step)
		add_child(step)
	

func _on_Celandro_place_footstep(location : Vector3, y : float):
	if pool.size() > 0:
		pool[0].rotation_degrees.y = y
		pool[0].translation = location
		pool[0].initiate()
		pool.remove(0)
		
	else:
		print("Footstep pool failed. I could have made it grow here but you know what, I love living on the edge.")