extends Spatial

onready var pin = preload('res://assets/ArenaSpawns/Pin.tscn')
onready var blockade = preload('res://assets/ArenaSpawns/Blockade.tscn')
onready var pillar = preload('res://assets/ArenaSpawns/Pillar.tscn')
onready var shoe = preload('res://assets/ArenaSpawns/Shoe.tscn')

var items : Array

var player_speed : float = 0.01

func _ready():
	items.append(pin)
	items.append(blockade)
	items.append(pillar)


func _on_Celandro_info(pos, rot) -> void:
	$Lanes.rotation.y = rot.y + PI
	
func create_item() -> void:
	randomize()
	items.shuffle()
	var new = items[0].instance()
	var lane
	lane = $Lanes.get_child(randi() % 3)
		
#	new.scale = Vector3(0.01,0.01,0.01)
	new.translation = lane.global_transform.origin
	new.rotation.y = $Lanes.rotation.y
	new.initiate()
	$Items.add_child(new)

func create_shoe() -> void:
	var new = shoe.instance()
	var lane
	lane = $Lanes.get_child(randi() % 3)
		
#	new.scale = Vector3(0.01,0.01,0.01)
	new.translation = lane.global_transform.origin
	new.rotation.y = $Lanes.rotation.y
	new.initiate()
	$Items.add_child(new)	

func _on_BuildTime_timeout():
	$BuildTime.wait_time = 2 - player_speed * 1.5
	var chance = randf()
	if chance > 0.8:
		create_item()
		yield(get_tree(), "idle_frame")
		create_item()
	else:
		create_item()


func _on_Celandro_speed(amount):
	player_speed = amount


func _on_ShoeTime_timeout():
	$ShoeTime.wait_time = rand_range(0.5, 5)
	create_shoe()
