extends Spatial

const config_file_path : = "user://config.cfg"

func save_stats() -> void:
	save_setting("player","times_jumped", $Celandro.times_jumped)
	save_setting("player","shoes_collected", $Celandro.shoes_collected)
	save_setting("player","objects_dodged", $Celandro.objects_dodged)
	
func final_save() -> void:
	var tj = load_setting("player","times_jumped", 0)
	var sc = load_setting("player","shoes_collected", 0)
	var od = load_setting("player","objects_dodged", 0)
	
	save_setting("player","times_jumped", tj + $Celandro.times_jumped)
	save_setting("player","shoes_collected", sc + $Celandro.shoes_collected)
	save_setting("player","objects_dodged", od + $Celandro.objects_dodged)
	
func load_setting(section, what, default):
	var conf = ConfigFile.new()
	conf.load(config_file_path)
	var value = conf.get_value(section, what, default)
	
	return value
	
func save_setting(section, what, value) -> void:
	var conf = ConfigFile.new()
	conf.load(config_file_path)
	conf.set_value(section, what, value)
	conf.save(config_file_path)
	
func end_game() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = true
	$GameOver.visible = true

func _on_OK_END_pressed():
	get_tree().paused = false
	final_save()
	yield(get_tree(), "idle_frame")
	get_tree().change_scene("res://scenes/MainMenu.tscn")
