extends ColorRect

var mouse_speed : Vector2
var mouse_smooth : Vector2
var orig_position : Vector2

var mouse_scale : float = 2.0

func _ready():
	orig_position = rect_position
	$MarginContainer/HBoxContainer/Instructions.connect("mouse_entered", self, "play_mouse_over")
	$MarginContainer/HBoxContainer/Stats.connect("mouse_entered", self, "play_mouse_over")
	$MarginContainer/HBoxContainer/Play.connect("mouse_entered", self, "play_mouse_over")
	$MarginContainer/HBoxContainer/Settings.connect("mouse_entered", self, "play_mouse_over")
	$MarginContainer/HBoxContainer/Credits.connect("mouse_entered", self, "play_mouse_over")

	$MarginContainer/HBoxContainer/Credits.connect("pressed", self, "play_click")
	$MarginContainer/HBoxContainer/Instructions.connect("pressed", self, "play_click")
	$MarginContainer/HBoxContainer/Stats.connect("pressed", self, "play_click")
	$MarginContainer/HBoxContainer/Play.connect("pressed", self, "play_click")
	$MarginContainer/HBoxContainer/Settings.connect("pressed", self, "play_click")
func _input(event):
	if event is InputEventMouseMotion:
		mouse_speed = event.relative

func _process(delta):
	mouse_smooth = mouse_smooth.linear_interpolate(mouse_speed * mouse_scale, 10 * delta)
#	self.rect_position.y = orig_position.y - mouse_smooth.y
	
	mouse_speed = Vector2()

func play_click() -> void:
	$MouseClick.play()
	
	
func play_mouse_over() -> void:
	$MouseOver.play()
	
	
func _on_ExitButton_pressed():
	play_click()
	yield($MouseClick, "finished")
	get_tree().quit()
	
	
func _on_Credits_pressed():
	get_parent().show_credits()
	play_click()
	
	
func _on_Settings_pressed():
	get_parent().show_options()
	play_click()
	
	
func _on_Play_pressed():
	get_parent().show_play()
	play_click()
	
	
func _on_Instructions_pressed():
	get_parent().show_how()
	play_click()
	
func _on_Stats_pressed() -> void:
	get_parent().show_stats()
	play_click()
	
	