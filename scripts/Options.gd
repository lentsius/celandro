extends VBoxContainer

var music : = 2
var sounds : = 2

var half_icon : StreamTexture = preload('res://ui/icons/volume-1.svg')
var on_icon : StreamTexture = preload('res://ui/icons/volume-2.svg')
var off_icon : StreamTexture = preload('res://ui/icons/volume-x.svg')

var icons : Array

const NM_DB : int = 0
const MEDIUM_DB : = -24
const NO_DB : = -120


func _ready():
	icons.append(off_icon)
	icons.append(half_icon)
	icons.append(on_icon)
	music = get_parent().load_setting("game", "music", 2)
	sounds = get_parent().load_setting("game", "sounds", 2)
	update_levels(0)
	update_levels(1)

#	update_icons()


func _on_MusicButton_pressed() -> void:
	change_volume()


func _on_EffectsButton_pressed() -> void:
	change_volume(false)
	

func change_volume(m : bool = true, change : int = -1) -> void:
	if m:
		if music > 0:
			music += change
		else:
			music = 2
		print(music)
	
	else:
		if sounds > 0:
			sounds += change
		else:
			sounds = 2
		print(sounds)

	save()
	update_icons()
	update_levels(0)
	update_levels(1)
		
		
func save() -> void:
	get_parent().save_setting("game", "music", music)
	get_parent().save_setting("game", "sounds", sounds)
	
	
# 0 for music and 1 for sounds
func update_levels(what : = 0) -> void:
	var volume : = 0
	var list : Array = [music, sounds]
	match list[what]:
		0:
			volume = NO_DB
		1:
			volume = MEDIUM_DB
		2:
			volume = NM_DB
	
	AudioServer.set_bus_volume_db(what + 1, volume)
	
	
func update_icons() -> void:
	$Music/MusicButton.texture_normal = icons[music]
	$Sound/EffectsButton.texture_normal = icons[sounds]