extends Node

var player_name : String = "PLAYER_NAME"

const config_file_path : String = "user://config.cfg"

#stats
var times_jumped : int
var lootboxes_collected : int
var times_died : int

func load_setting(section, what, default):
	var conf = ConfigFile.new()
	conf.load(config_file_path)
	var value = conf.get_value(section, what, default)
	
	return value
	
func save_setting(section, what, value) -> void:
	var conf = ConfigFile.new()
	conf.load(config_file_path)
	conf.set_value(section, what, value)
	conf.save(config_file_path)