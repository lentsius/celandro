extends Spatial

var speed : float = 0
var max_speed : int = 2

var player_rot : Vector3
var player_pos : Vector3
var smooth_rot : Vector3
var smooth_pos : Vector3

onready var cam : Camera = $Target/Camera
onready var target : Spatial = $Target

var mouse_speed : Vector2
var mouse_smooth : Vector2

var target_x : float = -60.775

var shaking : = false
var shake_strength : = 2.0

var half_shake : = false
var full_shake : = false

signal announce(message, col)

func _ready():
	pass # Replace with function body.

func _input(event):
	if event is InputEventMouseMotion:
		mouse_speed = event.relative

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	smooth_pos = smooth_pos.linear_interpolate(player_pos, 3 * delta)
	rotation = player_rot
	global_transform.origin.y = smooth_pos.y
	
	mouse_smooth = mouse_smooth.linear_interpolate(mouse_speed, 3 * delta)
	target.rotation_degrees += Vector3(mouse_smooth.y, -mouse_smooth.x, 0.0)
	mouse_speed = Vector2()
	
	if shaking:
		shake()

func shake() -> void:
	$Target/Camera.h_offset = rand_range(-shake_strength, shake_strength)
	$Target/Camera.v_offset = rand_range(-shake_strength, shake_strength)

func _on_Celandro_info(pos, rot):
	player_rot = rot
	player_pos = pos

func start_shake(amount : float = 2.00, duration : float = 2.0) -> void:
	shaking = true
	$Tween.interpolate_property(self, 'shake_strength', amount, 0, duration, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()
	yield($Tween, "tween_completed")
	shaking = false


func _on_Celandro_changed_lane(new_lane_number):
	match new_lane_number:
		0:
			target_x = -51.81
		1:
			target_x = -54.81
		2:
			target_x = -57.81
			
	var target_loc : Vector3 = target.translation
	target_loc.x = target_x
	
	$Tween.interpolate_property(target, 'translation', target.translation, target_loc, 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()

func _on_Celandro_jumped():
	pass # Replace with function body.


func _on_Celandro_speed(amount):
	if amount > 0.5 && half_shake == false:
		half_shake = true
		start_shake(2.0, 2.0)
		emit_signal("announce", "HALF SPEED", Color.blueviolet)
	elif amount > 0.99 && full_shake == false:
		full_shake = true
		start_shake(3.5,3.5)
		emit_signal("announce", "FULL SPEED", Color.coral)


func _on_Celandro_collided():
	half_shake = false
	full_shake = false
