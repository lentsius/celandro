extends Control

func _input(event):
	if event is InputEventKey:
		intro_end()

func intro_end() -> void:
	get_tree().change_scene("res://scenes/MainMenu.tscn")