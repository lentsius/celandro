extends Control

var paused : = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta) -> void:
	if Input.is_action_just_pressed("ui_cancel") && paused:
		get_back()
	elif Input.is_action_just_pressed("ui_cancel") && !paused:
		pause()

func pause() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	visible = true
	paused = true
	get_tree().paused = true
	var mat = $ColorRect.material
	$Tween.interpolate_property(mat, "shader_param/blur_amount", 0, 2.5, 4, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.interpolate_property(mat, "shader_param/overlay", Color(1,1,1,0), Color("#0e1b20"), 2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start()

func _on_RETURN_pressed():
	get_back()
	
func get_back() -> void:
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	self.visible = false
	paused = false
#	set_process(false)


func _on_RESTART_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()


func _on_EXIT_pressed():
	get_parent().final_save()
	yield(get_tree(), 'idle_frame')
	yield(get_tree(), 'idle_frame')	
	get_tree().quit()


func _on_MENU_pressed():
	get_parent().final_save()
	get_tree().paused = false
	get_tree().change_scene("res://scenes/MainMenu.tscn")
