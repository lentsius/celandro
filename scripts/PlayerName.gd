extends HBoxContainer

var editing : = false

func _on_Edit_pressed():
	if !editing:
		$HBoxContainer/Edit.visible = false
		$HBoxContainer/Save.visible = true
		editing = true
		$HBoxContainer/PName.editable = true
		$HBoxContainer/PName.grab_focus()
		$HBoxContainer/PName.caret_position = $HBoxContainer/PName.text.length()

func _on_Save_pressed():
	if editing:
		$HBoxContainer/Edit.visible = true
		$HBoxContainer/Save.visible = false
		editing = false
		$HBoxContainer/PName.editable = false
		get_parent().update_name()