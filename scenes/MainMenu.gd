extends Control

var center_pos : Vector2

var slides : Array

const trans_offset = 2000

const config_file_path : = "user://config.cfg"

var active_slide

func _ready():
	slides.append($Credits)
	slides.append($Options)
	slides.append($How)
	slides.append($Play)
	slides.append($Stats)
	
	center_pos = $Options.rect_position
	var ui_curve = load_setting("settings","ui_curve", 0.2)
	$CurvedShader.material.set_shader_param("distort_factor", ui_curve)
	
	$PlayerName/HBoxContainer/PName.text = load_setting("player", "name", "PLAYER NAME")
	globals.player_name = $PlayerName/HBoxContainer/PName.text
	update_name(false)
	$Options/Curve/CenterContainer/CurveSlider.value = $CurvedShader.material.get_shader_param("distort_factor")


func show_credits():
	hide_except(slides[0])
	var offset = Vector2(trans_offset, 0)
	$Tween.interpolate_property($Credits, 'rect_position', center_pos + offset, center_pos, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start() 
	active_slide = $Credits
	
	
func show_options():
	hide_except(slides[1])
	var offset = Vector2(trans_offset, 0)
	$Tween.interpolate_property($Options, 'rect_position', center_pos + offset, center_pos, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start() 
	active_slide = $Options
	
	
func show_how():
	hide_except(slides[2])
	var offset = Vector2(trans_offset, 0)
	$Tween.interpolate_property($How, 'rect_position', center_pos + offset, center_pos, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start() 
	active_slide = $How
	
	
func show_play():
	hide_except(slides[3])
	var offset = Vector2(trans_offset, 0)
	$Tween.interpolate_property($Play, 'rect_position', center_pos + offset, center_pos, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start() 
	active_slide = $Play
	
func show_stats():
	hide_except(slides[4])
	var offset = Vector2(trans_offset, 0)
	$Tween.interpolate_property($Stats, 'rect_position', center_pos + offset, center_pos, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tween.start() 
	active_slide = $Stats
	update_stats()
	
	
func hide_except(what) -> void:
	for i in slides:
		if i != what:
			i.visible = false
		else:
			i.visible = true
			
			
func _on_CurveSlider_value_changed(value):
	$CurvedShader.material.set_shader_param("distort_factor", value)
	save_setting("settings","ui_curve", value)
	
		
func load_setting(section, what, default):
	var conf = ConfigFile.new()
	conf.load(config_file_path)
	var value = conf.get_value(section, what, default)
	
	return value
	
	
func save_setting(section, what, value) -> void:
	var conf = ConfigFile.new()
	conf.load(config_file_path)
	conf.set_value(section, what, value)
	conf.save(config_file_path)
	
func update_name(save : bool = true) -> void:
	if save:
		save_setting("player", "name", $PlayerName/HBoxContainer/PName.text)
		globals.player_name = $PlayerName/HBoxContainer/PName.text
	$Play/Text.text = "Hey there " + $PlayerName/HBoxContainer/PName.text + " ready to possess as many shoes as possible?"

func _on_LezzDoIt_pressed():
	get_tree().change_scene("res://scenes/LeGameRoot.tscn")
	
func update_stats() -> void:
	var tj = load_setting("player","times_jumped", 0)
	var sc = load_setting("player","shoes_collected", 0)
	var od = load_setting("player","objects_dodged", 0)
	
	$"Stats/HBoxContainer/1/TJ".text = str(tj)
	$"Stats/HBoxContainer/2/SC".text = str(sc)
	$"Stats/HBoxContainer/3/OD".text = str(od)

func _on_RESET_pressed():
	save_setting("player","times_jumped", 0)
	save_setting("player","shoes_collected", 0)
	save_setting("player","objects_dodged", 0)
	update_stats()


func _on_BRAG_pressed():
	OS.shell_open("https://lentsius-bark.itch.io/celantro")


func _on_LezzDoIt2_pressed():
	OS.shell_open("www.soundimage.org")


func _on_KK_pressed():
	OS.shell_open("https://lentsius-bark.itch.io")
