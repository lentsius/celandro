tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("AnimatedSideButton", "ColorRect", preload("res://addons/AnimatedSideButton/AnimatedSideButton.gd"), preload("res://addons/AnimatedSideButton/Icon.png"))

func _exit_tree():
	# Erase the control from the memory
	remove_custom_type("AnimatedSideButton")

